package com.pratala.dikshatek.test.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresPermission
import androidx.fragment.app.Fragment
import com.pratala.dikshatek.test.R
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


object NetworkUtils {
    @RequiresPermission(Manifest.permission.ACCESS_NETWORK_STATE)
    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkInfo = cm.activeNetwork ?: return false
            val capability = cm.getNetworkCapabilities(networkInfo) ?: return false

            return when {
                capability.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) -> true
                else -> false
            }

        } else {
            val info = cm.activeNetworkInfo ?: return false
            return info.isConnected
        }
    }
}

fun onMain(task: () -> Unit) {
    CoroutineScope(Dispatchers.Main.immediate).launch {
        task.invoke()
    }
}

fun ImageView.setImage(imageUrl: String?){
//    Glide.with(context)
//        .load(Uri.parse(imageUrl))
//        .error(R.mipmap.ic_launcher)
//        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
//        .addListener(object : RequestListener<Drawable> {
//            override fun onLoadFailed(
//                e: GlideException?,
//                model: Any?,
//                target: Target<Drawable>?,
//                isFirstResource: Boolean
//            ): Boolean {
//                e?.logRootCauses("Checkin Error")
//                return false
//            }
//
//            override fun onResourceReady(
//                resource: Drawable?,
//                model: Any?,
//                target: Target<Drawable>?,
//                dataSource: DataSource?,
//                isFirstResource: Boolean
//            ): Boolean {
//                return false
//            }
//        })
//        .into(this)
    Picasso.get().load(Uri.parse(imageUrl)).error(R.mipmap.ic_launcher).into(this)
}

fun Fragment.toast(msg: String){
    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
}

fun Fragment.closeKeyboard(){
    val imm: InputMethodManager =
        requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view: View? = activity!!.currentFocus
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Fragment.showKeyboard(){
    val imm: InputMethodManager =
        requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

    imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS)
}
