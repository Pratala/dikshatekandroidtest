package com.pratala.dikshatek.test.model

enum class ViewState {
    Loading,Success,Fail
}