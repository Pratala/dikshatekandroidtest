package com.pratala.dikshatek.test.views.viewholders

import androidx.recyclerview.widget.RecyclerView
import com.pratala.dikshatek.test.adapters.AdapterItemListener
import com.pratala.dikshatek.test.databinding.PhotoSnippetBinding
import com.pratala.dikshatek.test.model.Photo
import com.pratala.dikshatek.test.utils.setImage

class PhotoViewHolders(
    private val bin : PhotoSnippetBinding,
    private val itemListener:AdapterItemListener<Photo>
):RecyclerView.ViewHolder(bin.root) {

    fun initView(photo: Photo){
        bin.apply {
            card.setOnClickListener {
                itemListener.onItemClick(item = photo, position = bindingAdapterPosition)
            }
            title.text = photo.title
            image.setImage(photo.thumbnailUrl)
        }
    }
}