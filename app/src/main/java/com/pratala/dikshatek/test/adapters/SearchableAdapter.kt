package com.pratala.dikshatek.test.adapters

interface SearchableAdapter {
    fun filter(key:String)
    fun onSearchClose()
}