package com.pratala.dikshatek.test.network

import okhttp3.CipherSuite
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.TlsVersion
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

abstract class RetrofitInstance : TimeOut {

    abstract val baseUrl: String
    override var timeOut: Long = 3000

    val get: Retrofit by lazy {

        if (baseUrl.isEmpty()) {
            throw IllegalAccessException("Empty Base URL")
        }

        Retrofit.Builder().apply {
            baseUrl(baseUrl)
            client(getClient())
            getConverter().forEach {
                addConverterFactory(it)
            }
        }.build()
    }

    protected open fun initSetting() {

    }

    protected open fun getClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(timeOut, TimeUnit.MILLISECONDS)
            .addInterceptor(interceptor())
            .build()
    }

    protected open fun getConverter(): ArrayList<Converter.Factory> {
        return ArrayList<Converter.Factory>().apply {
            add(GsonConverterFactory.create())
        }
    }

    protected open fun getTSLClient(): OkHttpClient {
        val spec = ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
            .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0)
            .cipherSuites(
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_DHE_RSA_WITH_AES_256_GCM_SHA384,
                CipherSuite.TLS_DHE_RSA_WITH_AES_256_CBC_SHA,
                CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384,
                CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
                CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA
            )
            .build()
        return OkHttpClient.Builder()
            .connectionSpecs(
                listOf(spec)
            )
            .addInterceptor(interceptor())
            .readTimeout(timeOut, TimeUnit.MILLISECONDS)
            .build()
    }

    private fun interceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
            this.redactHeader("Pratala-Test")
        }
    }
}