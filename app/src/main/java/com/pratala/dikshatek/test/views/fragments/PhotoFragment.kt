package com.pratala.dikshatek.test.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.pratala.dikshatek.test.databinding.PhotoFragmentBinding
import com.pratala.dikshatek.test.utils.setImage

class PhotoFragment: Fragment() {

    private val args : PhotoFragmentArgs by navArgs()

    private val bin : PhotoFragmentBinding by lazy {
        PhotoFragmentBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return bin.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bin.apply {
            args.photo.let { photo ->
                title.text = photo.title
                image.setImage(photo.url)
            }
        }
    }
}