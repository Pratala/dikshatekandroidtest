package com.pratala.dikshatek.test.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.pratala.dikshatek.test.room.PhotoLocal

data class Photo(
    @SerializedName(Key.Album.ID)
    val albumId:Long,
    @SerializedName(Key.Photo.ID)
    val id:Long,
    @SerializedName(Key.Photo.TITLE)
    val title:String?,
    @SerializedName(Key.Photo.URL)
    val url:String?,
    @SerializedName(Key.Photo.THUMBNAIL_URL)
    val thumbnailUrl:String?
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(albumId)
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeString(url)
        parcel.writeString(thumbnailUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Photo> {
        override fun createFromParcel(parcel: Parcel): Photo {
            return Photo(parcel)
        }

        override fun newArray(size: Int): Array<Photo?> {
            return arrayOfNulls(size)
        }

        fun convertAll(localPhoto:List<PhotoLocal>):List<Photo>{
            val mList = ArrayList<Photo>(localPhoto.count())
            localPhoto.forEach {
                mList.add(
                    Photo(
                        id = it.id,
                        title = it.title,
                        url = it.url,
                        thumbnailUrl = it.thumbnailUrl,
                        albumId = it.albumId
                    )
                )
            }

            return mList
        }
    }

    fun isContainSame(other:Photo):Boolean{
        return albumId == other.albumId && title == other.title && url == other.url && thumbnailUrl == other.thumbnailUrl
    }

    fun isItemSame(other:Photo):Boolean{
        return id == other.id
    }
}