package com.pratala.dikshatek.test.views.fragments

import androidx.fragment.app.Fragment
import com.pratala.dikshatek.test.views.activities.MainActivity

open class BaseFragment: Fragment() {
    protected fun loading(loading: MainActivity.Loading){
        val tempActivity = requireActivity()

        if(tempActivity is MainActivity){
            tempActivity.loading(loading)
        }
    }
}