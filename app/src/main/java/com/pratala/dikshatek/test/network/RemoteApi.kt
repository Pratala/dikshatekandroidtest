package com.pratala.dikshatek.test.network

import com.pratala.dikshatek.test.model.Photo
import retrofit2.Response
import retrofit2.http.GET

interface RemoteApi {
    @GET(EndPoint.PHOTOS)
    suspend fun photos(): Response<List<Photo>>
}