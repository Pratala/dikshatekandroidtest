package com.pratala.dikshatek.test.network

object ApiInstance:RetrofitInstance() {
    override val baseUrl: String = "https://jsonplaceholder.typicode.com/"

    val api: RemoteApi by lazy {
        get.create(RemoteApi::class.java)
    }
}