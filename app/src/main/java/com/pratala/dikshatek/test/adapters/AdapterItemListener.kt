package com.pratala.dikshatek.test.adapters

interface AdapterItemListener<T> {
    fun onItemClick(item:T, position:Int)
}