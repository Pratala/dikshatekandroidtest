package com.pratala.dikshatek.test.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.pratala.dikshatek.test.model.Photo
import com.pratala.dikshatek.test.model.Table

@Entity(tableName = Table.Photo.TABLE)
data class PhotoLocal(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = Table.Photo.ID)
    var id: Long,
    @ColumnInfo(name =Table.Photo.ALBUM_ID)
    var albumId: Long,
    @ColumnInfo(name = Table.Photo.TITLE)
    var title: String,
    @ColumnInfo(name = Table.Photo.URL)
    var url: String,
    @ColumnInfo(name = Table.Photo.THUMBNAIL)
    var thumbnailUrl: String
){
    companion object{
        fun convert(list:List<Photo>):List<PhotoLocal>{
            val mList = ArrayList<PhotoLocal>(list.size)
            list.forEach {
                mList.add(
                    PhotoLocal(
                        id = it.id,
                        albumId = it.albumId,
                        title = it.title ?: "",
                        url = it.url ?: "",
                        thumbnailUrl = it.thumbnailUrl ?: ""
                    )
                )
            }
            return mList
        }
    }
}
