package com.pratala.dikshatek.test.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pratala.dikshatek.test.databinding.PhotoSnippetBinding
import com.pratala.dikshatek.test.model.Photo
import com.pratala.dikshatek.test.views.viewholders.PhotoViewHolders

class PhotoAdapter(
    private val itemClickListener : AdapterItemListener<Photo>
): RecyclerView.Adapter<PhotoViewHolders>(),SearchableAdapter {
    private var itemList : List<Photo>? = null
    private var tempItemList : List<Photo>? = null

    fun setItems(items:List<Photo>){
        this.itemList = items
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolders {
        val inflater = LayoutInflater.from(parent.context)
        val bin = PhotoSnippetBinding.inflate(inflater)
        return PhotoViewHolders(
            bin = bin,
            itemListener = itemClickListener
        )
    }

    override fun onBindViewHolder(holder: PhotoViewHolders, position: Int) {
        itemList?.get(position)?.let { photo->
            holder.initView(photo = photo)
        }
    }

    override fun getItemCount(): Int {
        return itemList?.count() ?: 0
    }

    override fun filter(key: String) {
        itemList?.let { sItemList->
            tempItemList = sItemList.toMutableList()
            tempItemList?.filter {
                it.title?.contains(key) ?: false
            }.let { result->
                setItems(result ?: emptyList())
            }
        }
    }

    override fun onSearchClose() {
        tempItemList?.let{
            if(it.size != itemList?.size ?: 0){
                setItems(it)
            }
        }
    }
}