package com.pratala.dikshatek.test.repo

import com.pratala.dikshatek.test.model.Photo
import com.pratala.dikshatek.test.network.ApiInstance
import com.pratala.dikshatek.test.network.RemoteApi
import retrofit2.Response

class Repo {
    private val api : RemoteApi by lazy {
        ApiInstance.api
    }

    suspend fun getRemotePhotos(): Response<List<Photo>> {
        return api.photos()
    }
}