package com.pratala.dikshatek.test.views.fragments

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.pratala.dikshatek.test.R
import com.pratala.dikshatek.test.adapters.AdapterItemListener
import com.pratala.dikshatek.test.adapters.PhotoAdapter
import com.pratala.dikshatek.test.databinding.MainFragmentBinding
import com.pratala.dikshatek.test.model.Photo
import com.pratala.dikshatek.test.model.PhotoData
import com.pratala.dikshatek.test.model.ViewState
import com.pratala.dikshatek.test.utils.closeKeyboard
import com.pratala.dikshatek.test.utils.showKeyboard
import com.pratala.dikshatek.test.utils.toast
import com.pratala.dikshatek.test.viewmodels.PhotoViewModel
import com.pratala.dikshatek.test.views.activities.MainActivity


class MainFragment: BaseFragment() {

    private val vm : PhotoViewModel by viewModels()
    private var searchView : SearchView? = null

    private val bin : MainFragmentBinding by lazy {
        MainFragmentBinding.inflate(layoutInflater)
    }

    private val adapter : PhotoAdapter by lazy {
        PhotoAdapter(
            itemClickListener = itemClickListener
        )
    }

    private val searchTextListener = object : SearchView.OnQueryTextListener {

        override fun onQueryTextSubmit(query: String?): Boolean {
            query?.takeIf {
                it.isNotEmpty()
            }?.let { sQuery->
                adapter.filter(sQuery)
            }
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            //do nothing
            return true
        }
    }

    private val onItemExpandListener = object  : MenuItem.OnActionExpandListener{
        override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
            item!!.actionView.requestFocus()
            showKeyboard()
            return true
        }

        override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
            adapter.onSearchClose()
            closeKeyboard()
            return true
        }
    }

    private val itemClickListener : AdapterItemListener<Photo> by lazy {
        object : AdapterItemListener<Photo>{
            override fun onItemClick(item: Photo, position: Int) {
                MainFragmentDirections.actionMainFragmentToPhotoFragment(item).let {
                    findNavController().navigate(it)
                }
            }
        }
    }

    private val observer : Observer<PhotoData> = Observer {
        when(it.state){
            ViewState.Loading -> {
                loading(MainActivity.Loading.Show)
            }
            ViewState.Fail -> {
                loading(MainActivity.Loading.Hide)
                it.message?.let { msg ->
                    toast(msg)
                }
            }
            ViewState.Success -> {
                loading(MainActivity.Loading.Hide)
                it.data?.let { photos ->
                    adapter.setItems(photos)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return bin.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bin.apply {
            rv.adapter = adapter
        }

        vm.photoLD.observe(viewLifecycleOwner, observer)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        vm.photoLD.removeObserver(observer)
    }

    override fun onResume() {
        super.onResume()
        vm.getPhotos(requireContext())
        closeKeyboard()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val searchItem = menu.findItem(R.id.search)

        if(searchItem != null){
            val searchManager =
                requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView = searchItem.actionView as SearchView
            searchView!!.apply {
                setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName));
                setOnQueryTextListener(searchTextListener)
                isFocusable = true
                isIconified = false
            }

            searchItem.setOnActionExpandListener(
                onItemExpandListener
            )
        }
    }
}