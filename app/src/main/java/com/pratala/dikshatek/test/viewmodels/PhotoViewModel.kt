package com.pratala.dikshatek.test.viewmodels

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.pratala.dikshatek.test.model.ErrorMessage
import com.pratala.dikshatek.test.model.Photo
import com.pratala.dikshatek.test.model.PhotoData
import com.pratala.dikshatek.test.model.ViewState
import com.pratala.dikshatek.test.repo.Repo
import com.pratala.dikshatek.test.room.AppDatabase
import com.pratala.dikshatek.test.room.PhotoLocal
import com.pratala.dikshatek.test.utils.NetworkUtils
import kotlinx.coroutines.launch

class PhotoViewModel : ViewModel() {
    private val _photoLD: MutableLiveData<PhotoData> = MutableLiveData()
    private var localDb: AppDatabase? = null

    private val remoteData: Repo by lazy {
        Repo()
    }

    val photoLD: LiveData<PhotoData> = Transformations.switchMap(_photoLD) {
        object : LiveData<PhotoData>() {
            override fun onActive() {
                super.onActive()
                value = it
            }
        }
    }

    fun getPhotos(context: Context) {
        viewModelScope.launch {
            _photoLD.postValue(PhotoData(
                    state = ViewState.Loading
            ))

            localDb = AppDatabase.getInstance(context)

            val localData = localDb!!.dao().getAllPhoto()

            if(localData.isEmpty()){
                val response = getRemotePhotos(context)
                _photoLD.postValue(response)
                tryUpdateLocalData(response,localData)
            }else{
                Log.w("Test","From Local")
                Photo.convertAll(localData).let {
                    _photoLD.postValue(
                        PhotoData(
                            state = ViewState.Success,
                            data = it
                        )
                    )
                }
            }
        }
    }

    private suspend fun tryUpdateLocalData(response: PhotoData, localData: List<PhotoLocal>) {
        if(response.data != null && response.data.isNotEmpty()){
            if(localData.isEmpty()){
                localDb?.dao()?.insertPhotos(PhotoLocal.convert(response.data))
            }
        }
    }

    private suspend fun getRemotePhotos(context: Context): PhotoData {
        NetworkUtils.isNetworkAvailable(context = context).apply {
            return if(this){
                val response = remoteData.getRemotePhotos()
                 if (response.isSuccessful) {
                    PhotoData(
                        state = ViewState.Success,
                        data = response.body()
                    )
                }else{
                    PhotoData(
                        state = ViewState.Fail,
                        message = ErrorMessage.FAIL_GET_DATA
                    )
                }
            }else{
                PhotoData(
                    state = ViewState.Fail,
                    message = ErrorMessage.NO_CONNECTION
                )
            }
        }
    }
}