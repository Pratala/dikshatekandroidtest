package com.pratala.dikshatek.test.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [PhotoLocal::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase: RoomDatabase() {
    abstract fun dao() :AppDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getInstance(
            context: Context,
            dbName: String = "QnesysDatabase",
        ): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: createInstance(
                    context = context,
                    dbName = dbName
                ).also {
                    instance = it
                }
            }
        }

        private fun createInstance(
            context: Context,
            dbName: String
        ): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, dbName).build()
        }
    }
}