package com.pratala.dikshatek.test.views.activities

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.pratala.dikshatek.test.R
import com.pratala.dikshatek.test.views.dialog.LoadingDialog

class MainActivity: AppCompatActivity() {
    private var dialog : LoadingDialog? = null

    enum class Loading{
        Show,Hide
    }

    fun loading(loading: Loading){
        if (dialog == null){
            dialog = LoadingDialog(this)
        }

        if(loading == Loading.Show){
            dialog?.showDialog()
        }else{
            dialog?.hideDialog()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}