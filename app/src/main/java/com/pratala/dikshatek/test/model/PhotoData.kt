package com.pratala.dikshatek.test.model

data class PhotoData(
    val state:ViewState,
    val message:String? = null,
    val data:List<Photo>? = null
){
    override fun toString(): String {
        return "State : $state, Message: ${message ?: "Message Empty"}, Date: ${data?.count() ?: 0}"
    }
}