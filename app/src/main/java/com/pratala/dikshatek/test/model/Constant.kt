package com.pratala.dikshatek.test.model

object Key{
    object Photo{
        const val ID = "id"
        const val TITLE = "title"
        const val URL = "url"
        const val THUMBNAIL_URL = "thumbnailUrl"
    }

    object Album{
        const val ID = "albumId"
    }
}

object ErrorMessage{
    const val NO_CONNECTION = "No Connection"
    const val FAIL_GET_DATA = "Fail Get Data"
}

object Table{
    object Photo{
        const val TABLE = "TbPhoto"
        const val ID = "ID"
        const val ALBUM_ID = "albumId"
        const val TITLE = "title"
        const val URL = "url"
        const val THUMBNAIL = "thumbnailUrl"
    }
}