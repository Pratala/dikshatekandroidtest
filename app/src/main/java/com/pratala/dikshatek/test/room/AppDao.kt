package com.pratala.dikshatek.test.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
abstract class AppDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = PhotoLocal::class)
    abstract suspend fun insertPhotos(products: List<PhotoLocal>)

    @Query("SELECT * FROM TbPhoto")
    abstract suspend fun getAllPhoto(): List<PhotoLocal>
}