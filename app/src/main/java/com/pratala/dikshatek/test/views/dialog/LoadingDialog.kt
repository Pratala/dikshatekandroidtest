package com.pratala.dikshatek.test.views.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.pratala.dikshatek.test.R

class LoadingDialog(
    activity: AppCompatActivity,
    @DrawableRes
    imageRes:Int? = null
) {

    private val dialog = AlertDialog.Builder(activity)
        .setView(activity.layoutInflater.inflate(R.layout.loading_fragment, null))
        .setCancelable(false)
        .create().apply {
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

    fun showDialog() {
        if(dialog.isShowing){
            dialog.dismiss()
        }
        dialog.show()
    }

    fun hideDialog(){
        dialog.dismiss()
    }
}